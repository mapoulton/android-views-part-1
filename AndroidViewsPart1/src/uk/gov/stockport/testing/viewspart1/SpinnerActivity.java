package uk.gov.stockport.testing.viewspart1;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class SpinnerActivity extends Activity implements OnItemSelectedListener {
	
	ArrayList<String> pData;
	Spinner pSpinner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_spinner);
		
		setupData();
		setupSpinner();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private void setupSpinner(){
		pSpinner = (Spinner) findViewById(R.id.spinner);
		
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, 
				android.R.layout.simple_spinner_item,
				android.R.id.text1,
				pData);
		
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		pSpinner.setAdapter(adapter);
		
		pSpinner.setOnItemSelectedListener(this);
	}
	
	private void setupData(){
		pData = new ArrayList<String>();
		
		pData.add("Martin");
		pData.add("Jon");
		pData.add("Clare");
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
}
