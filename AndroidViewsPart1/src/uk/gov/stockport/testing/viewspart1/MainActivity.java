package uk.gov.stockport.testing.viewspart1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	Button pOnClick;
	Button pNewButtonListener;
	Button pPublicOverlaodedListener;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		pOnClick = (Button) findViewById(R.id.button_onclick);
		pNewButtonListener = (Button) findViewById(R.id.button_newbuttonlistener);
		pPublicOverlaodedListener = (Button) findViewById(R.id.button_publicoverloaded);
		
		// pOnClick has already had its onClick listener set in the layout.
		pNewButtonListener.setOnClickListener(ourOnClickListener);
		pPublicOverlaodedListener.setOnClickListener(this);
		
		ToastMessage("Main Activity Loaded");
	}
	
	private OnClickListener ourOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			ToastMessage("New On click listener attached");
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void onClick(View v){
		switch(v.getId()){
		case R.id.button_publicoverloaded:
			ToastMessage("Public overload clicked");
			
			// This loads the next view, spinner activity 
			Intent i = new Intent(this, SpinnerActivity.class);
			startActivity(i);
			break;
		}
	}
	
	public void onLayoutClick(View v){
		switch(v.getId()){
		case R.id.button_onclick:
			ToastMessage("on click function pressed");
			break;
		}
	}

	private void ToastMessage(String message){
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}
}
